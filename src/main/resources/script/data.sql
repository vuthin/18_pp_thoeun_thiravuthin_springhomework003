
create table authors(
    author_id   serial primary key not null,
    author_name varchar(50),
    gender      varchar(50)
);


create table categories(
    category_id serial primary key,
    category_name  varchar(255)
);


create table books(
    book_id        serial primary key not null,
    title          varchar(255),
    published_date timestamp,
    author_id      integer,
    FOREIGN KEY (book_id) references authors (author_id) on update cascade on delete cascade
);


create table book_details(
    id  serial primary key not null,
    book_id integer references books on update cascade on delete cascade,
   category_id integer references categories on update cascade on delete cascade
);

