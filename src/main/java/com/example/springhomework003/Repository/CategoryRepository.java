package com.example.springhomework003.Repository;

import com.example.springhomework003.Models.entity.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("""
            
            SELECT  c.category_id, c.category_name
            FROM categories c INNER JOIN book_details ON c.category_id = book_details.category_id
            WHERE book_id= #{id};
            """)
    @Result(property = "categoryId", column = "category_id")
     List<Category> getAllByBookId(@Param("id") Integer bookId);
}
