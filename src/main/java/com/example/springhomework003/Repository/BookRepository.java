package com.example.springhomework003.Repository;

import com.example.springhomework003.Models.entity.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {

    // get all book
    @Select("""
            SELECT * FROM books;
            """)
    @Result(property = "bookId", column = "book_id")
    @Result(property = "publishedDate", column = "published_date")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.example.springhomework003.Repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "category", column = "category_id",
            many = @Many(select = "com.example.springhomework003.Repository.CategoryRepository.getAllByBookId")
    )
    List<Book> getAllBook();
}
