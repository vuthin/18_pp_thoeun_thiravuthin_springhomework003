package com.example.springhomework003.Repository;

import com.example.springhomework003.Models.request.AuthorRequest;
import com.example.springhomework003.Models.entity.Author;
import org.apache.ibatis.annotations.*;

import java.util.List;

/*
    + AuthorRepository Handel with database, Use query for fetch database from database.
    + We use @annotation on the class is  @Mapper.
    +
*/
@Mapper
public interface AuthorRepository {
    //  get all
    @Select("""
            SELECT * FROM authors
            """)
    // We use result for mapping between class and table database
    // property = NamingOfClass and  column = namingOfColumn database
    @Result(property = "authorId", column = "author_id")
    @Result(property = "authorName", column = "author_name")
    List<Author> getAllAuthor();

    //  get by id
    @Select("""
            SELECT * FROM authors 
            WHERE author_id = #{id};
            """)
    @Result(property = "authorId", column = "author_id")
    @Result(property = "authorName", column = "author_name")
    Author getAuthorById(Integer id);

    //  insert
        //    @Insert("""
        //            insert into authors ( author_name, gender)
        //            VALUES (#{authors.name}, #{authors.gender});
        //            """)
    @Select("""
            INSERT INTO authors ( author_name, gender)
            VALUES (#{authors.authorName}, #{authors.gender})
            RETURNING *
            """)
    @Result(property = "authorId", column = "author_id")
    @Result(property = "authorName", column = "author_name")
    Author insertAuthor(@Param("authors") Author authors);


    //    Update
    @Update("""
            UPDATE authors
            SET authorName =#{authors.authorName}, gender=#{authors.gender}
            WHERE author_id = #{id}
            """)
    void updateAuthor(Integer id,@Param("author") AuthorRequest authorRequest);


    // delete
    @Delete("""
            DELETE FROM authors
            WHERE author_id =#{id};
            """)
    void deleteAuthorById(Integer id);
}
