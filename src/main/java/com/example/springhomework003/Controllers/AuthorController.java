package com.example.springhomework003.Controllers;

import com.example.springhomework003.Models.response.ApiResponse;
import com.example.springhomework003.Models.request.AuthorRequest;
import com.example.springhomework003.Models.entity.Author;

import com.example.springhomework003.service.AuthorService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/*
    +   Controller call service --> so inject service into controller
    +   We use @annotation @RestController on the class
    +   @ResController use for request and response with http browser client
 */

@RestController
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }


    // fetch all data
    @GetMapping("/api/v1/authors")
    public List<Author> getAllAuthors(){
        return authorService.getAllAuthor();
    }


    // fetch by ID
    // We should use pathVariable when work with id
    @GetMapping("/api/v1/authors{id}")
    public ResponseEntity<?> getAuthorById(@PathVariable Integer id ){
        Author authors =  authorService.getAuthorById(id);
        ApiResponse<Author> apiResponse = new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.NOT_FOUND,
                "Successfully fetched author",
                authors
        );
        return ResponseEntity.ok(apiResponse);
    }


    // Insert or post data
    @PostMapping("/api/v1/authors")
    public ResponseEntity<?> insertAuthor(@RequestBody Author authors){
            return ResponseEntity.ok(new ApiResponse<Author>(
                    LocalDateTime.now(),
                    HttpStatus.OK,
                    "Successfully added author",
                    (Author) authorService.insertAuthor(authors)
            ));
    }


    // Update data
    @PutMapping("/api/v1/authors/{id}")
    public ResponseEntity<?> updateAuthor(@PathVariable Integer id, @RequestBody AuthorRequest authorRequest){
        authorService.updateAuthor(id,authorRequest);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully updated author",
                null
        ));
    }


    // Delete data
    @DeleteMapping("/api/v1/authors/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable Integer id){
        authorService.deleteAuthorById(id);
        return ResponseEntity.ok(new ApiResponse<>(
                LocalDateTime.now(),
                HttpStatus.OK,
                "Successfully deleted author",
                null
        ));
    }
}
