package com.example.springhomework003.service;

import com.example.springhomework003.Models.entity.Book;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {

    List<Book> getAllBook();
}
