package com.example.springhomework003.service.serviceImpl;

import com.example.springhomework003.Models.request.AuthorRequest;
import com.example.springhomework003.Models.entity.Author;
import com.example.springhomework003.Repository.AuthorRepository;
import com.example.springhomework003.service.AuthorService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;


// --- We inject Repository into AuthorServiceImpl

@Component
@Repository
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer id) {
        return authorRepository.getAuthorById(id);
    }

    @Override
    public Author insertAuthor(Author authors) {
        return authorRepository.insertAuthor(authors);
    }

    @Override
    public void updateAuthor(Integer id, AuthorRequest authorRequest) {
        authorRepository.updateAuthor(id,authorRequest);
    }

    @Override
    public void deleteAuthorById(Integer id) {
        authorRepository.deleteAuthorById(id);
    }


}
