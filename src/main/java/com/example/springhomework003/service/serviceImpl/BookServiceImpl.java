package com.example.springhomework003.service.serviceImpl;

import com.example.springhomework003.Models.entity.Book;
import com.example.springhomework003.Repository.BookRepository;
import com.example.springhomework003.service.BookService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
@Component
@Repository
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBook() {
        return bookRepository.getAllBook();
    }
}
