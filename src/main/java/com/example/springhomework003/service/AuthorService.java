package com.example.springhomework003.service;

import com.example.springhomework003.Models.request.AuthorRequest;
import com.example.springhomework003.Models.entity.Author;
import org.springframework.stereotype.Service;

import java.util.List;
/*
     +  Service it just contain the abstract method
     +
 */

@Service
public interface AuthorService {

    List<Author> getAllAuthor();


    Author getAuthorById(Integer id);

    Author insertAuthor(Author authors);

    void updateAuthor(Integer id, AuthorRequest authorRequest);

    void deleteAuthorById(Integer id);
}
