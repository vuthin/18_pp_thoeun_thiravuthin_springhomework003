package com.example.springhomework003.exception;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String  message) {
        super(message);
    }
}
