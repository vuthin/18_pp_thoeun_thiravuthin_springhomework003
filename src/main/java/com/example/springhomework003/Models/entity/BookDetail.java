package com.example.springhomework003.Models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookDetail {
    private int id;
    private Book bookId;
    private Category categoryId;
}
