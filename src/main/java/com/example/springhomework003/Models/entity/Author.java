package com.example.springhomework003.Models.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
    + Class models is the for hold the data
    + We use @annotation 3 is : @Data,  @AllArgsConstructor, @NoArgsConstructor
    +
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Author {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer authorId;
    private String authorName;
    private String gender;
}
