package com.example.springhomework003.Models.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    private Integer bookId;
    private String title;
    private Timestamp publishedDate;
    private Author author;
    private List<Category> category;

}
