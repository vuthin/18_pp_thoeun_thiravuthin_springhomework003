package com.example.springhomework003.Models.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse<T> {
    private LocalDateTime time;
    private HttpStatus status;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL) // if null not show data
    private T Payload;
}
